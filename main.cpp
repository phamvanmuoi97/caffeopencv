#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/dnn.hpp>
#include <tuple>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <iterator>
using namespace cv;
using namespace cv::dnn;
using namespace std;

const size_t inWidth = 224;
const size_t inHeight = 224;
const double inScaleFactor = 1.0;
const cv::Scalar meanVal(37, 37, 37);



const std::string caffeConfigFile = "../VGG_FACE_deploy.prototxt";
const std::string caffeWeightFile = "../Weights.caffemodel";


void detectFaceOpenCVDNN(Net net, Mat &frameOpenCVDNN)
{
	

    cv::Mat inputBlob = cv::dnn::blobFromImage(frameOpenCVDNN, inScaleFactor, cv::Size(inWidth, inHeight), meanVal, false, false);
    net.setInput(inputBlob);
    cout<<"111"<<endl;
    cv::Mat detection = net.forward("fc7");
    cout<<"222"<<endl;
    cv::Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());
    cout<<"333"<<endl;
   
}


int main(int argc, char** argv)
{
    Net net = cv::dnn::readNetFromCaffe(caffeConfigFile, caffeWeightFile);
    Mat frame= imread("../bs000_N_N_0.bmp");

    while(1)
  	{
      
      detectFaceOpenCVDNN ( net, frame );
      imshow( "OpenCV", frame );
      int k = waitKey(5);
      if(k == 27)
      {
        destroyAllWindows();
        break;
      }
    }

}
